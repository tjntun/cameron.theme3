jQuery(document).ready(function($) {
    // Theme picker
    var themeColor = localStorage.getItem('cameron_theme_color');
    if (themeColor == null) { themeColor = 0; }
    var themeBg = localStorage.getItem('cameron_theme_bg');
    if (themeBg == null) { themeBg = 1; }
    changeTheme(themeColor, themeBg);
    initMap(themeColor);

    $('#editTools .picker .group').click(function() {
        var themeColor = $(this).attr('theme-color');
        var themeBg = $(this).attr('theme-bg');
        changeTheme(themeColor, themeBg);

        localStorage.setItem('cameron_theme_color', themeColor);
        localStorage.setItem('cameron_theme_bg', themeBg);
    });

    function changeTheme(themeColor, themeBg) {
        // Mark color pallete as selected
        $('#editTools .group').removeClass('selected');
        $('#editTools .group[theme-color="'+themeColor+'"][theme-bg="'+themeBg+'"]').addClass('selected');

        $('body').removeClass('theme-color-0 theme-color-1 theme-color-2 theme-color-3 theme-color-4').addClass('theme-color-' + themeColor);
        $('body').removeClass('theme-bg-1 theme-bg-2').addClass('theme-bg-' + themeBg);
        initMap(themeColor);
    }

    // Load default font
    var fontBig = localStorage.getItem('cameron_font_big');
    if (fontBig != null) {
        $('.font-big').css('font-family', fontBig);
        $('#font-big-select [value=' + fontBig + ']').attr('selected', true);
    } else {
        fontBig = $('#font-big-select').val();
        $('.font-big').css('font-family', fontBig);
    }

    var fontSmall = localStorage.getItem('cameron_font_small');
    if (fontSmall != null) {
        $('.font-small').css('font-family', fontSmall);
        $('#font-small-select [value=' + fontSmall + ']').attr('selected', true);
    } else {
        fontSmall = $('#font-small-select').val();
        $('.font-small').css('font-family', fontSmall);
    }

    // Font picker
    $('#font-big-select').change(function(event) {
        var font = $(this).val();
        $('.font-big').css('font-family', font);
        localStorage.setItem('cameron_font_big', font);
    });
    $('#font-small-select').change(function(event) {
        var font = $(this).val();
        $('.font-small, .content ul, p').css('font-family', font);
        localStorage.setItem('cameron_font_small', font);
    });

    // Nicescroll
    $("html").niceScroll();
    if (!isMobile.any) $(".niceScroll").niceScroll();

    // Home section height
    if (isMobile.any) {
        $('#home').height(window.innerHeight);
    }

    // Re-position features item
    if($(window).width() < 568) {
        $('#features .sidebar.left').parent().insertAfter('#features .main');
    }

    // Mobile menu
    var MobileMenu = $("#mobile-nav").mmenu({
        offCanvas: {
            position: "right",
        },
        navbar: {
            title: ''
        },
    }).data('mmenu');

    $('.hamburger').click(function(event) {
        event.preventDefault();
        if ($('#mobile-nav').hasClass('mm-opened')) {
            MobileMenu.close();
        } else {
            MobileMenu.open();
        }
    });

    // Sticky nav
    $(window).on('scroll', function(event) {
        stickyNav();
        updateActiveNav();
    });
    stickyNav();

    function stickyNav() {
        // if ($(window).width() > 1200) {
        var scrollTop = $(window).scrollTop();
        if (scrollTop >= $('#features').offset().top) {
            $('#secondary-nav').addClass('sticky');
        } else {
            $('#secondary-nav').removeClass('sticky');
        }
        // }
    }

    updateActiveNav();

    // All section height

    if (!isMobile.any) {
        $('#wrapper > section').not('#contact').css('min-height', $(window).height() + 'px');
        $(window).resize(function(event) {
            $('#wrapper > section').not('#contact').css('min-height', $(window).height() + 'px');
        });
    }else{
        $('#gallery').css('min-height', $(window).height() + 'px');
    }
    

    // Active nav
    var currentPageId = 'home';

    function updateActiveNav() {
        var scrollTop = $(window).scrollTop();
        $('#wrapper > section').each(function(index, el) {
            var id = $(this).attr('id');
            var adjust = 80;
            if (scrollTop >= ($(this).offset().top - adjust) && scrollTop <= ($(this).offset().top + $(this).outerHeight())) {
                $('nav a').removeClass('active');
                $('nav a[href=#' + id + ']').not('.logo').addClass('active');
            }
        });
    }

    // Next/prev section
    $('#secondary-nav .icon-arrowdown').click(function() {
        // next
        var nextPageId = $('#' + currentPageId).next().attr('id');
        scrollTo($('#' + nextPageId));
    });
    $('#secondary-nav .icon-arrowup').click(function() {
        // back
        var prevPageId = $('#' + currentPageId).prev().attr('id');
        scrollTo($('#' + prevPageId));
    });

    // Smooth scroll
    $('a[href*="#"]:not([href="#"])').click(function() {
        stickyNav();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            return scrollTo(target);
        }
    });

    function scrollTo(target) {
        MobileMenu.close();
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        var adjust = 80;
        if(target.attr('id') == 'features') adjust = 0;
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top - adjust
            }, 1000);
            return false;
        }
        return false;
    }

    // Square gallery
    try {
        $('#gallery .bxslider').bxSlider({
            auto: true,
            pager: false,
            nextSelector: '#gallery-next',
            prevSelector: '#gallery-prev',
            // nextText: '', 
            // prevText: '',
            touchEnabled: true
        });
    } catch (e) {
        console.log(e)
    }

    // Map
    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        try {
            if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
            if (mapId == undefined || mapId == null) mapId = "map";
            var place = { lat: 54.515102, lng: -128.610764 };
            map = new google.maps.Map(document.getElementById(mapId), {
                center: place,
                zoom: 13,
                scrollwheel: false,
                styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
            });
            var icon = 'img/icon/marker' + themeNumber + '.svg?t=' + Date.now();
            var marker = new google.maps.Marker({
                position: place,
                map: map,
                icon: icon
            });
        } catch (e) {
            console.log('Google map API is missing');
        }
    }
});
